//
//  ProductListController+Protocols.swift
//  Elements
//
//  Created by Yanstar Studio on 10/02/2017.
//  Copyright © 2017 Yanstar Studio. All rights reserved.
//

import UIKit

protocol UpdateProductListDelegate: class {
    func updateUISwitchValueForProduct(at itemIndex: Int, to switchIsOn: Bool)
}

// -------------------------------
// MARK: Protocol Implementation
// -------------------------------
extension ProductListController: UpdateProductListDelegate {
    func updateUISwitchValueForProduct(at itemIndex: Int, to switchIsOn: Bool) {
        products?[itemIndex].switchIsOn = switchIsOn
    }
}

// ---------------------------------
// MARK: UICollectionView Protocols
// ---------------------------------
extension ProductListController: UICollectionViewDelegateFlowLayout {
    
    // UICollectionView Datasource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductListCell
        cell.product = products?[indexPath.item]
        cell.tag = indexPath.item
        cell.delegate = self
        return cell
    }
    
    // UICollectionView Delegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productDetailSlidingController = ProductDetailController(collectionViewLayout: UICollectionViewFlowLayout())
        productDetailSlidingController.productsArray = products
        productDetailSlidingController.selectedIndex = indexPath.item
        navigationController?.present(productDetailSlidingController, animated: true, completion: nil)
    }
    
    // CollectionView Delegate Flowlayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let product = products?[indexPath.item]
        if let productTitle = product?.productTitle {
            // total width - 4 * gap of 8 - left image - right switch
            let estimatedFrame = estimateFrameForText(text: productTitle, width: Int(view.frame.width) - 4 * 8 - 80 - 51, height: 1000, font: UIFont.systemFont(ofSize: 18)).height + 24
            
            let height = estimatedFrame > 100 ? estimatedFrame : 100
            return CGSize(width: view.frame.width, height: height)
        }
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
    }
}
