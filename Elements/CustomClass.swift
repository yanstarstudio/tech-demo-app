//
//  CustomClass.swift
//  Elements
//
//  Created by Yanstar Studio on 06/02/2017.
//  Copyright © 2017 Yanstar Studio. All rights reserved.
//

import UIKit
import DataCache

// MARK: Load and Cache image from URL imageView
class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    func loadImageUsingUrlString(urlString: String?) {
        
        imageUrlString = urlString
        self.image = #imageLiteral(resourceName: "placeholder_noImage")
        
        guard let string = urlString else { return }
        
        if let imageFromCache = DataCache.instance.readImageForKey(key: string){
            self.image = imageFromCache
            return
        }
        
        ApiService.sharedInstance.fetchDataFromUrlString(string) { (data, errorString) in
            
            guard errorString == nil, let data = data else { return }
            
            DispatchQueue.main.async{
                // This check is to fix that the 1st time when image is loading, due to slow internet or large images, sometimes, the image is loaded in a wrong position in collectionview or tableview.
                if self.imageUrlString == urlString {
                    self.image = UIImage(data: data) == nil ? #imageLiteral(resourceName: "placeholder_noImage") : UIImage(data: data)
                }
            }
            
            // Cache imageData
            guard let imageToCache = UIImage(data: data) else {return}
            DataCache.instance.write(image: imageToCache, forKey: string)
        }
    }
}
