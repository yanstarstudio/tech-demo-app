//
//  ViewController.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit
import DataCache

class ProductListController: UICollectionViewController {

    let cellId = "celId"
    var products: [Product]?
    var refresher:UIRefreshControl!
    var cachedData: Data?

    // ---------------------------
    // MARK: View Life Cycle
    // ---------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Gallery"
        setupCollectionView()
        setupPullRefresher()
        loadProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    // ---------------------------
    // MARK: Setup Views
    // ---------------------------
    fileprivate func setupPullRefresher() {
        refresher = UIRefreshControl()
        refresher.tintColor = .darkGray
        refresher.addTarget(self, action: #selector(handleReload), for: .valueChanged)
        collectionView?.addSubview(refresher)
    }
    
    fileprivate func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ProductListCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    // ---------------------------
    // MARK: Load data
    // ---------------------------
    func loadProducts() {
        
        // If cached product is available, load cached products first.
        cachedData = DataCache.instance.readData(forKey: DataCacherConstants.CSVKey)
        if let csvData = cachedData {
            Product.convertDataToProducts(data: csvData, completion: { (products, errorString) in
                guard errorString == nil else {
                    DispatchQueue.main.async {
                        self.displayError(errorString: errorString!)
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    self.products = products
                    self.collectionView?.reloadData()
                }
            })
        }
        // then load data from url, reload collectionview if data changed, refresh cache
        handleReload()
    }
    
    // load products from url
    func handleReload() {
        ApiService.sharedInstance.fetchDataFromUrlString(DataURLConstants.urlString) { (data, errorString) in
            guard errorString == nil, let data = data else {
                DispatchQueue.main.async {
                    self.stopRefresher()
                    delay(seconds: 0.1, completion: {
                        self.displayError(errorString: errorString!)
                    })
                }
                return
            }
            
            // If cached data is not the same as online data, reload collectionview and renew the cache
            if self.cachedData != data {
                Product.convertDataToProducts(data: data, completion: { (products, errorString) in
                    if errorString == nil {
                        DispatchQueue.main.async {
                            self.stopRefresher()
                            self.products = products
                            self.collectionView?.reloadData()
                        }
                    }
                })
                DataCache.instance.write(data: data, forKey: DataCacherConstants.CSVKey)
            } else {
                DispatchQueue.main.async {
                    self.stopRefresher()
                }
            }
        }
    }
    
    // ---------------------------
    // MARK: Helper
    // ---------------------------

    func displayError(errorString: String) {
        if errorString == ErrorStrings.HTTPClientError.rawValue {
            alertViewWithOneButton(viewController: self, alertTitle: "Oops", alertMessage: "Please double-check your internet connection and authorization", cancelButtonTitle: "OK")
        } else  {
            alertViewWithOneButton(viewController: self, alertTitle: "Oops", alertMessage: "The file may have moved or the server is unavailable. Please come back later.", cancelButtonTitle: "OK")
        }
    }
    
    func stopRefresher() {
        if refresher.isRefreshing {
            refresher.endRefreshing()
        }
    }
}




