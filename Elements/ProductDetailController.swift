//
//  ProductDetailController.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit

class ProductDetailController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    var productsArray: [Product]?
    var selectedIndex = 0
    
    lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "buttonClose"), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    // ---------------------------
    // MARK: View Life Cycle
    // ---------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        floatingButton()
        
        let scrollToIndexPath = IndexPath(item: selectedIndex, section: 0)
        collectionView?.scrollToItem(at: scrollToIndexPath, at: .centeredHorizontally, animated: true)
        collectionView?.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let notificationName = Notification.Name(rawValue: NotificationNames.UpdateProductList)
        NotificationCenter.default.post(name: notificationName, object: productsArray)
    }
    
    // ---------------------------
    // MARK: Action Handlers
    // ---------------------------
    func handleClose() {
        dismiss(animated: true, completion: nil)
    }
    
    // ---------------------------
    // MARK: Setup Views
    // ---------------------------
    func setupCollectionView() {
        // Enable horizontal scrolling
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView?.backgroundColor = .black
        collectionView?.showsHorizontalScrollIndicator = false
        
        collectionView?.register(ProductDetailCell.self, forCellWithReuseIdentifier: cellId) // register cell
        collectionView?.isPagingEnabled = true // snap to cells while scrolling
    }
    
    func floatingButton() {
        view.addSubview(closeButton)
        _ = closeButton.anchor(top: nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 6, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        closeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // ---------------------------
    // MARK: CollectionView
    // ---------------------------
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsArray?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductDetailCell
        cell.product = productsArray?[indexPath.item]
        cell.tag = indexPath.item
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 50)
    }
}

// -------------------------------
// MARK: Protocol Implementation
// -------------------------------
extension ProductDetailController: UpdateProductListDelegate {
    func updateUISwitchValueForProduct(at itemIndex: Int, to switchIsOn: Bool) {
        productsArray?[itemIndex].switchIsOn = switchIsOn
    }
}
