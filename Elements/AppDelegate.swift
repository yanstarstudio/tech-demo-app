//
//  AppDelegate.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // Setup RootViewController as ProductListController
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let rootViewController = ProductListController(collectionViewLayout: UICollectionViewFlowLayout())
        window?.rootViewController = UINavigationController(rootViewController: rootViewController)
        
        setupBarStyles(application: application)
        return true
    }
    
    // Navigation bar, status bar, tab bar styles
    func setupBarStyles(application: UIApplication) {
        
        UINavigationBar.appearance().barTintColor = UIColor.themeColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 20, weight: UIFontWeightBold)]
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = true
        application.statusBarStyle = .lightContent
    }

}

