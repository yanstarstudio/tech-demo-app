//
//  Common.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit

// Basic Alert View
func alertViewWithOneButton(viewController: UIViewController, alertTitle: String?, alertMessage: String?, cancelButtonTitle: String, completion: (() -> Void)? = nil) {
    
    let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    viewController.present(alertController, animated: true, completion: completion)
}

// Estimate the size of a String
func estimateFrameForText(text: String, width: Int, height: Int, font: UIFont) -> CGRect {
    
    let size = CGSize(width: width, height: height)
    let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
    let rect = NSString(string: text).boundingRect(with: size,
                                                   options: option,
                                                   attributes: [NSFontAttributeName: font],
                                                   context: nil)
    return rect
}

// A delay function
func delay(seconds: Double, completion:@escaping ()->()) {
    let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) {
        completion()
    }
}
