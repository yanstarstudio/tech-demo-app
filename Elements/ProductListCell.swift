//
//  ProductListCell.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit

class ProductListCell: BaseCell {
    
    var product: Product? {
        didSet {
            guard let product = product else {
                return
            }
            imageView.loadImageUsingUrlString(urlString: product.productImageUrlString)
            titleLabel.text = product.productTitle
            productSwitch.isOn = product.switchIsOn
        }
    }
    
    weak var delegate: UpdateProductListDelegate?
    
    lazy var imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.themeColor()
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        return label
    }()
    
    // A divider line at the bottom of the cell
    let dividerLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.themeColor()
        return view
    }()
    
    lazy var productSwitch: UISwitch = {
        let mySwitch = UISwitch()
        mySwitch.setOn(false, animated: false)
        mySwitch.clipsToBounds = true
        mySwitch.layer.cornerRadius = 15
        mySwitch.addTarget(self, action: #selector(switchChanged(sender:)), for: .valueChanged)
        return mySwitch
    }()
    
    func switchChanged(sender: UISwitch!) {
        delegate?.updateUISwitchValueForProduct(at: tag, to: sender.isOn)
    }

    override func setupViews() {
        super.setupViews()
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(dividerLineView)
        addSubview(productSwitch)
        
        imageView.anchorWithConstantsToTop(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        imageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1.0).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        _ = productSwitch.anchor(top: nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        productSwitch.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        productSwitch.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, for: .horizontal)
        
        titleLabel.anchorWithConstantsToTop(topAnchor, left: imageView.rightAnchor, bottom: nil, right: productSwitch.leftAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8)
        titleLabel.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, for: .horizontal)


        dividerLineView.anchorToTop(top: bottomAnchor, left: imageView.leftAnchor, bottom: nil, right: rightAnchor)
        dividerLineView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        titleLabel.textColor = UIColor.themeColor()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
    }
}


