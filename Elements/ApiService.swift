//
//  ApiService.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import Foundation

class ApiService: NSObject {
    
    static let sharedInstance = ApiService()
    
    func fetchDataFromUrlString(_ urlString: String, completion: @escaping (_ products: Data?, _ error: String?) -> ()) {

        guard let url = URL(string: urlString) else {
            completion(nil, ErrorStrings.URLError.rawValue)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard (error == nil) else {
                _ = (error! as NSError).code == -1009 ? completion(nil, ErrorStrings.HTTPClientError.rawValue) : completion(nil, ErrorStrings.DataTaskError.rawValue)
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                
                switch statusCode {
                    case 400...499:
                        completion(nil, ErrorStrings.HTTPClientError.rawValue)
                        return
                    case 200...299:
                        break
                    default:
                        completion(nil, ErrorStrings.HTTPClientError.rawValue)
                        return
                }
            }
            
            guard let data = data else {
                completion(nil, ErrorStrings.DataNonExisting.rawValue)
                return
            }
            completion(data, nil)
        }.resume()
    }
}















