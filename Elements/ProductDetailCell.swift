//
//  ProductDetailCell.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import UIKit

class ProductDetailCell: ProductListCell {

    override var product: Product? {
        didSet {
            guard let product = product else {
                return
            }
            imageView.loadImageUsingUrlString(urlString: product.productImageUrlString)
            titleLabel.text = product.productTitle
            if let productDetailText = product.productDetail {
                detailWebView.loadHTMLString(productDetailText, baseURL: nil)
            }
            productSwitch.isOn = product.switchIsOn
        }
    }
    
    let detailWebView: UIWebView = {
        let detailWebView = UIWebView()
        detailWebView.isUserInteractionEnabled = false
        detailWebView.backgroundColor = .clear
        detailWebView.isOpaque = false
        return detailWebView
    }()
    
    let textContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    override func switchChanged(sender: UISwitch!) {
        delegate?.updateUISwitchValueForProduct(at: tag, to: sender.isOn)
    }
    
    override func setupViews() {
        
        addSubview(imageView)
        addSubview(textContainerView)
        
        imageView.anchorToTop(top: topAnchor, left: nil, bottom: textContainerView.topAnchor, right: nil)
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9).isActive = true
        
        textContainerView.anchorWithConstantsToTop(nil, left: imageView.leftAnchor, bottom: bottomAnchor, right: imageView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        textContainerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
        
        setupContainerView()

        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        backgroundColor = .black
    }
    
    func setupContainerView() {
        textContainerView.addSubview(titleLabel)
        textContainerView.addSubview(detailWebView)
        textContainerView.addSubview(productSwitch)
        
        titleLabel.anchorWithConstantsToTop(textContainerView.topAnchor, left: textContainerView.leftAnchor, bottom: nil, right: textContainerView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8)
        titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFontWeightBold)
        
        _ = productSwitch.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: textContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        productSwitch.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, for: .horizontal)
        
        detailWebView.anchorToTop(top: titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: textContainerView.bottomAnchor, right: productSwitch.rightAnchor)
    }

}
