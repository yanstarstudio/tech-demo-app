//
//  Product.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import Foundation

class Product: NSObject {
    
    var productTitle: String?
    var productDetail: String?
    var productImageUrlString: String?
    var switchIsOn: Bool
    
    init(title: String, productDetail: String, productImageUrl: String?, switchIsOn: Bool = false) {
        self.productTitle = title
        self.productDetail = productDetail
        self.productImageUrlString = productImageUrl
        self.switchIsOn = switchIsOn
    }
    
    static func convertDataToProducts(data: Data, completion: @escaping (_ products: [Product]?, _ error: String?) -> ()) {
        
        let datastring = String(data: data, encoding: .utf8)
        
        guard let unwrappedStringToArray = datastring?.csvRows() else {
            completion(nil, ErrorStrings.CSVUnwrappingError.rawValue)
            return
        }
        var products = [Product]()
        
        for (index, row) in unwrappedStringToArray.enumerated() {
            if index != 0 {
                let product = Product(title: row[0],
                                      productDetail: row[1],
                                      productImageUrl: row.count == 3 ? row[2] : nil)
                
                products.append(product)
            }
        }
        completion(products, nil)
    }

}
