//
//  Constants.swift
//  Elements
//
//  Created by Yanstar Studio on 18/12/2016.
//  Copyright © 2016 Yanstar Studio. All rights reserved.
//

import Foundation

// Potential Error Cases

enum ErrorStrings: String {
    case URLError = "URL doesn't exisit"
    case HTTPClientError = "It appears that sth went wrong on your internet connection"
    case HTTPServerError = "Server is down!"
    case DataNonExisting = "No data was returned by the request!"
    case DataTaskError = "there was an error with the request"
    case CSVUnwrappingError = "CSVUnwrappingError"
}

struct DataCacherConstants {
    static let CSVKey = "csv"
}

struct NotificationNames {
    static let UpdateProductList = "updateProductList"

}

struct DataURLConstants {
    static let urlString = "https://firebasestorage.googleapis.com/v0/b/csvhosting.appspot.com/o/test_application_data%20-%20Sheet%201.csv?alt=media&token=7d8b3a2a-9145-4a41-a34b-4a09b1925ef5"
}
