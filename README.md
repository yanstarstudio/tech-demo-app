# README #

### Technical Demo Example: Completed in a few hours for demo purpose ###

* A Demo app is created to show customer csv data hosted in backend. This file can be downloaded from ([Download Link](https://firebasestorage.googleapis.com/v0/b/csvhosting.appspot.com/o/test_application_data%20-%20Sheet%201.csv?alt=media&token=7d8b3a2a-9145-4a41-a34b-4a09b1925ef5))
 - **CSV file**: The file contains images urls (some broken, some with no image and some with images), image description and image detail. Some descriptions are longer than others and certain image detail has html font formatting.
 - **App**: 1. Upon entering the app, a list of all images + title + detail are shown.
            2. Upon clicking any row, a bigger image is shown along with its title and description. User can swipe left and right to view more images. 
            3. User can toggle on/off switch. The state of the switch is persisted for each app launch


       ![Screenshots.png](https://bitbucket.org/repo/jzqjng/images/2991566972-Screenshots.png)

### How do I get set up? ###

* Please navigate to the product folder in terminal. Run "pod install" and open the created .xcworkspace file.
* The current app supports iOS 10 and up. 

### Some Remarks ###

* I have chosen UICollectionView to display the list of image along with titles. The main reason behind is to keep the flexibility if different layout (eg. Pinterest style) is to be used in the future.

* I have used an existing csv unwrapper to convert csv to native objects. More attention should be put into this part to make csv -> object converting more robust, especially if the customer accidentally deletes a title or deletes the csv header... A better solution will be to provide the customer a more robust way to host and modify data instead of using csv.

* Caching. Currently, each time we download csv data, we save a cache copy if data is different from the previously saved version. I have thought about using coredata as local data store. However, I rejected this idea because in this current csv scheme, we are not utilizing powerful features of core data to track, modify each each product. 

* Image Caching. Each image is cached and can be referred to with its url key. I have created a CustomUIImageView class to perform this action.

* Error Handling. Users don't need to see the exact reason an action failed. So I have decided only to present alertview when something wrong happened on the user side (eg. no internet connection) or when sth wrong happened to the server side (eg. server is not responding)